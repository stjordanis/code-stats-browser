<img src="https://www.bit01.de/wp-content/uploads/2020/02/codestats_header.png" width="320" alt="Code Stats Browser Extension for Firefox, Chrome and Edge" />

# CodeStats Browser Extension
![Chrome Uses](https://img.shields.io/chrome-web-store/users/ljecglgmlhpjalfccaojplcidlmaochd?label=chrome%20users) ![FireFox Uses](https://img.shields.io/amo/users/code-stats-extension?label=firefox%20users&logoColor=orange) ![Version](https://img.shields.io/chrome-web-store/v/ljecglgmlhpjalfccaojplcidlmaochd) ![Version](https://img.shields.io/amo/v/code-stats-extension) 

FireFox and Chrome Extension for CodeStats page. Collects programming experience!

Sends programmed **code experience (XP)** from almost all major web editors and IDE to CodeStats.net. In CodeStats you can collect programming experience. The programming language is **detected completely automatically**.

Supported programming languages: *Python, PHP, HTML, JavaScript, JSON, CSS, Typescript, Java, Plain text*.

## How to install

**Chrome / Edge**
1. Install the extension via [Chrome Web Store](https://chrome.google.com/webstore/detail/code-stats-chrome-extensi/ljecglgmlhpjalfccaojplcidlmaochd?hl=de).
2. Put your [Machine API key](https://codestats.net/my/machines) into extension settings, after clicking on the "C::S" extension button.

**FireFox**
1. Install the extension via [FireFox Web Store](https://addons.mozilla.org/de/firefox/addon/code-stats-extension/?src=search).
2. Put your [Machine API key](https://codestats.net/my/machines) into extension settings, after clicking on the "C::S" extension button.

## Supported web editors

- CodeMirror (for e.g. Jupyter Notebook)
- Monaco Editor
- Ace Editor

## Questions

If you have any questions, feel free to [contact me](https://www.bit01.de/kontakt/) or request a pull of your source code.

## What is Code::Stats?

A free stats tracking service for programmers. You will be awarded with experience points for the amount of programming you do. Watch as your levels grow for each language you use. Identify your strong skill sets and use the data to see where you still have room for improvement.

Exmaple profile: [bitnulleins](https://codestats.net/users/bitnulleins)

## Licence

Source code is under MIT licence.
